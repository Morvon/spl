require([
    "splunkjs/mvc",
    "splunkjs/mvc/searchmanager",
    "splunkjs/splunk",
    // require our bundle js file
    "/static/app/km/config/dist/bundle.js",
  ], function (mvc, SearchManager, splunk_js_sdk, React) {
  
    // Start our React.js application and pass props in it 
    React.start({
      text: "Pass from Splunk to react app anything you need...",
      SearchManager,
      mvc,
      splunk_js_sdk,
      app_name : "ing_snow_rest"
    });
  
});
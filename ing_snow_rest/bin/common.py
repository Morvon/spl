class Credentials():
    def __init__(self, data):
        for row in data:
            if(row.realm=="KM_snow_rest_snow"):
                self.snow = User(row)
            elif(row.realm=="KM_snow_rest_proxy"):
                self.proxy = User(row)
            else:
                pass

class User():
    def __init__(self, data):
        self.username=data.username
        self.password=data.clear_password



import sys,os
sys.path.insert(0, os.path.sep.join([os.path.dirname(os.path.realpath(os.path.dirname(__file__))),'lib']))
from splunklib.searchcommands import dispatch, StreamingCommand, Configuration
from splunklib.client import connect
import pysnow
import requests
sys.path.insert(0, os.path.sep.join([os.path.dirname(os.path.realpath(os.path.dirname(__file__))),'bin']))
from common import Credentials


@Configuration()
class CreateIncSnowRest(StreamingCommand):
    def stream(self, records):

        service = self.service
        secrets = service.storage_passwords
        c = Credentials(secrets.list())
        
        file = open("../logi.txt", "a")
        #file.write("SNOW: USERNAME: "+c.snow.username+" PASSWORD CLEAR: "+c.snow.password+"\n")
        #file.write("PROXY: USERNAME: "+c.proxy.username+" PASSWORD CLEAR: "+c.proxy.password+"\n")

        """
        |stats count
        |eval ci="CI", comment="EMPTY",snowId="",snowRef="snowRef", date=now()
        |restcreateinc
        """
        s = requests.Session()
        s.proxies.update({'https': 'http://<address>'})
        #s.auth = requests.auth.HTTPBasicAuth('<username>', '<password>')

        #sn = pysnow.Client(instance='<instance>', session=s)
        #c = pysnow.Client(instance='myinstance', user='myusername', password='mypassword')
        # Define a resource, here we'll use the incident table API
        #incident = c.resource(api_path='/table/incident')
        # Query for incidents with state 1
        #response = incident.get(query={'number': 'INC012345'})
        # Iterate over the result and print out `sys_id` of the matching records.
        #file.write("resoponse: "+response)
        for record in records:
            file.write("record[ci]: " +record["ci"] +";")
            file.write("record[comment]: " +record["comment"] +";")
            file.write("record[snowId]: " +record["snowId"] +";")
            file.write("record[snowRef]: " +record["snowRef"] +";")
            file.write("record[date]: " +record["date"] +"\n")
            yield record



        

if __name__ == "__main__":
    dispatch(CreateIncSnowRest, sys.argv, sys.stdin, sys.stdout, __name__)
